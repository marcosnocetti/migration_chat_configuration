const db = require('./database.js')

module.exports = () => {
    return { getConfig }
    async function updateConfig(result, schemaName) {
        const newJson = buildChatRate(result.config);
        const migratedJSON = JSON.stringify(newJson)
        return db.none(`INSERT INTO ${schemaName}.chat_configuration(origins, config_name, config) values($2, 'CHAT_RATE_V2', $1) `, [migratedJSON, result.origins])
    }

    async function getConfig(schemaName) {
        const result = await db.oneOrNone(`SELECT config, origins FROM ${schemaName}.chat_configuration WHERE config_name = 'CHAT_RATE'`)
        if (result) {
            updateConfig(result, schemaName)
        } else {
            console.log
        }
    }

    function buildChatRate(config) {
        return {
            chatRate: {
                sendChatRate: config.chatRate.sendChatRate,
                questions:[ {
                    type: "TEXTUAL_RATE",
                    message: config.chatRate.chatRateConfig.message,
                    validAnswers: config.chatRate.chatRateConfig.texts,
                    ratingWindow: config.chatRate.chatRateConfig.ratingWindow,
                    commentQuestion: config.chatRate.chatRateConfig.commentQuestion
                }],
                feedbackMessage: config.chatRate.chatRateConfig.commentAnswer
            }
        }
    }
}




