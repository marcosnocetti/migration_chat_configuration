const pgp = require('pg-promise')()
const { dbURL } = require('./config.js')

const db = pgp(dbURL)

module.exports = db
