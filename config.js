const config = {}


config.dbHost = process.env.DB_HOST || 'ubots-hlg-12.co7s6dbmqtbd.us-east-1.rds.amazonaws.com'
config.dbUser = process.env.DB_USER || 'chatbots'
config.dbPassword = process.env.DB_PASSWORD || '4k*6xeE*vhAn'
config.dbDatabase = process.env.DB_DATABASE || 'chatbots'
config.dbPort = process.env.DB_PORT || 5432
config.dbURL =
    process.env.DB_URL ||
    `postgres://${config.dbUser}:${config.dbPassword}@${config.dbHost}:${config.dbPort}/${config.dbDatabase}`

module.exports = config
