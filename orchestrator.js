const migration = require('./migration')()

const schemasNames = [
    "ubots_api"
]

try {
    schemasNames.forEach(async schemaName => {
       await migration.getConfig(schemaName)
    })
} catch(err){
    console.log(err)
}